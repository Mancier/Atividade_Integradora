Unit calc_logical;

{
    In this block, we have two kinds of functions/procedures
     => Public: Could be invocaded outside of the unit
     => Private: Couldn't be incovated outside

     The INTERFACE block, I set the publics functions, unti_construct. This will be called in the main.pas

     The IMPLEMATION block, It's set only private function.
}

Interface
    
Uses
    SysUtils, Crt;

Type
    TArrayShortInt = Array [0..7] of ShortInt;
    TString = String[8];

    procedure verifica_TCC(verifica_resultado: TArrayShortInt);
    procedure calc_logical_construct();
    function not_operator(valorNegado: TArrayShortInt): TArrayShortInt;
    function and_operator(primeiro_byte_and, segundo_byte_and: TString): TArrayShortInt;
    function or_operator(primeiro_byte_or, segundo_byte_or: TString): TArrayShortInt; 
    function nand_operator(primeiro_byte_nand, segundo_byte_nand: TString): TArrayShortInt;
    function xor_function(primeiro_byte_xor, segundo_byte_xor :TString): TArrayShortInt;
    function xnor_function(primeiro_byte_xnor, segundo_byte_xnor: TString): TArrayShortInt;
    function nor_function(primeiro_byte_nor, segundo_byte_nor: TString): TArrayShortInt;


Implementation
    
    // ============================ Funções Auxiliares ============================
    procedure exibe_resultado(resultado_final: TArrayShortInt);
    var
        i: Integer;

    begin
        writeln;
        writeln('=================== Resultado ===================');
        write('Resultao da operacao => ');        

        for i := High(resultado_final) downto Low(resultado_final) do
        begin
            write(resultado_final[i]);
        end;
        writeln;        
        verifica_TCC(resultado_final);

        read;
    end;

    function not_operator(valorNegado: TArrayShortInt): TArrayShortInt;
    Var
        i: integer;

    begin
        for i := High(valorNegado) downto Low(valorNegado) do
        begin
            if valorNegado[i] = 1 then
                begin
                    not_operator[i] := 0;
                end
            else
                begin
                    not_operator[i] := 1;
                end;
        end; 
    end;

    procedure calc_logical_construct();
    Var
        primeiro_byte_start, segundo_byte_start: TString;
        option_selected: ShortInt;
        quit: boolean = false;
    begin
        repeat
        ClrScr; //Limpando a tela
        writeln('=================== CALCULADORA LOGICA ===================');

        write('Primeiro valor binario: ');
        readln(primeiro_byte_start);

        write('Segundo valor binario: ');
        readln(segundo_byte_start);
        
        writeln;

        writeln('=================== Selecione uma Opcao ===================');
            writeln('1 => AND');
            writeln('2 => NAND');       
            writeln('3 => OR');
            writeln('4 => NOR');           
            writeln('5 => XOR');
            writeln('6 => XNOR');
            writeln('0 => Sair');
            writeln();
            write('Selecione uma das opcoes acima: ');
            readln(option_selected);

            case option_selected of
                1:exibe_resultado(and_operator(primeiro_byte_start, segundo_byte_start));
                2:exibe_resultado(nand_operator(primeiro_byte_start, segundo_byte_start));
                3:exibe_resultado(or_operator(primeiro_byte_start, segundo_byte_start));
                4:exibe_resultado(nor_function(primeiro_byte_start, segundo_byte_start));
                5:exibe_resultado(xor_function(primeiro_byte_start, segundo_byte_start));
                6:exibe_resultado(xnor_function(primeiro_byte_start, segundo_byte_start));
                0: quit := true;
            end;
        until (quit=true);
    end;    

    procedure verifica_TCC(verifica_resultado: TArrayShortInt);
    Var
        i, maioria_um, maioria_zero: integer;

    begin
        {Zerando as variavaeis}
        maioria_zero := 0;
        maioria_um := 0;

        //Vamos verificar se é TAUTOLOGIA, CONTIGÊNCIA ou CONTRADIÇÃO
        for i := Low(verifica_resultado) to High(verifica_resultado) do
        begin
            if(verifica_resultado[i] = 1) then
            begin
                maioria_um := maioria_um + 1;
            end
            else
            begin
                maioria_zero := maioria_zero + 1;
            end;
        end;

        writeln('1: ', maioria_um, ' || ', '0: ', maioria_zero);

        //Apos somado os valores
        //Contingencia; bits = 0
        if (maioria_zero = length(verifica_resultado)) then
        begin
            writeln('Esse resultado e uma CONTIGENCIA');
        end;

            //TALTOLOGIA é quando os 8 bits = 1
        if (maioria_um = length(verifica_resultado)) then
        begin
            writeln('Esse resultado e uma TALTOLOGIA');
        end;
        
        if (maioria_um <> length(verifica_resultado)) and (maioria_zero <> length(verifica_resultado)) then
        begin
            writeln('Esse resultado e uma CONTRADICAO');
        end;

        readln;
    end;
    //============================ END ============================


    //============================ Funções Principais ============================
   
   //AND => Function
   function and_operator(primeiro_byte_and, segundo_byte_and: TString): TArrayShortInt;

    Var
        i: integer;

   begin
        for i := High(and_operator) downto Low(and_operator) do
        begin
            if (primeiro_byte_and[i] = '1') and (segundo_byte_and[i] = '1') then
                begin
                    and_operator[i] := 1;
                end
            else
                begin
                    and_operator[i] := 0;
                end;
        end;        
   end;
   
   //OR => Function
   function or_operator(primeiro_byte_or, segundo_byte_or: TString): TArrayShortInt; 

    Var
        {resultado_final_or: TArrayShortInt;}
        i: integer;

   begin
        for i := High(or_operator) downto Low(or_operator) do
        begin
            if (primeiro_byte_or[i] = '1') or (segundo_byte_or[i] = '1') then
                begin
                    or_operator[i] := 1;
                end
            else
                begin
                    or_operator[i] := 0;
                end;
        end; 
   end;

   // XOR => Function
   function xor_function(primeiro_byte_xor, segundo_byte_xor :TString): TArrayShortInt;
   Var
    i:integer;

   begin
       for i := High(xor_function) downto Low(xor_function) do
       begin
            if (primeiro_byte_xor[i] = '1') and (segundo_byte_xor = '1') then
                begin
                    xor_function[i] := 0;
                end
           else
                begin
                    xor_function[i] := 1;    
                end;
       end;
   end;

   // Negação da Funções
    //NAND => Function
    function nand_operator(primeiro_byte_nand, segundo_byte_nand: TString): TArrayShortInt;

    begin
        nand_operator := not_operator(and_operator(primeiro_byte_nand, segundo_byte_nand));
    end;

   //XNOR => Function
    function xnor_function(primeiro_byte_xnor, segundo_byte_xnor: TString): TArrayShortInt;

    Var 
    resultado_xor: TArrayShortInt;

    begin
        resultado_xor := xor_function(primeiro_byte_xnor, segundo_byte_xnor);
        xnor_function := not_operator(resultado_xor);
    end;

    //NOR => Function
    function nor_function(primeiro_byte_nor, segundo_byte_nor: TString): TArrayShortInt;
    begin
        nor_function := not_operator(or_operator(primeiro_byte_nor, segundo_byte_nor));
    end;
   // ============================ END ============================
END.
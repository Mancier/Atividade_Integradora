Unit Convertions;
Interface

    Uses 
        SysUtils, Crt, Math;

    Type
        TArrayShortInteger = Array [0..7] of ShortInt;
        TString = String[8];

    //Public Procedures/Functions
    function decimal_to_binary(decimal_value: integer): TArrayShortInteger;
    function binary_to_decimal(bd_decimal_number:String): Real;
    function somatoria_init(): TArrayShortInteger;
    procedure convertion_construct();
    procedure exibe_binario(exibir_result: TArrayShortInteger);
    procedure exibe_decimal(dec_exibir: Real);

Implementation

    procedure exibe_binario(exibir_result: TArrayShortInteger);
    Var 
        i: integer;

    begin
        write('Resultado: ');
        for i := High(exibir_result) downto Low(exibir_result) do
        begin
            write(exibir_result[i]);
        end;
        readln();
    end;

    procedure exibe_decimal(dec_exibir: Real);
    begin
        write('Resultado: ', dec_exibir:0:0);
        readln();
    end;

    procedure convertion_construct();
    Var
        option_selected: ShortInt;
        quit: boolean = false;
        decimal_value:integer;
        bd_decimal_number:String;

    begin
        writeln('=================== CONVERSAO ===================');
        writeln();
        repeat
            ClrScr;
            writeln('=================== Selecione uma Opcao ===================');
            writeln('1 => Decimal -> Binario');
            writeln('2 => Binario -> Decimal');
            writeln('0 => Sair');
            write('Seleciosne uma das opcoes acima: ');
            readln(option_selected);

            case option_selected of
                1: 
                begin
                    repeat
                        write('Numero Decimal: ');
                        readln(decimal_value);
                            
                            if decimal_value > 255  then
                                writeln('O numero digita extrapola os 8 bits');            
                    until (decimal_value < 255);

                    exibe_binario(decimal_to_binary(decimal_value));   
                end;

                2:
                begin
                    //Entrando com o dado
                    write('Digite o numero binario: ');
                    //Evitando que o dado supere os 8-bit
                    repeat
                        readln(bd_decimal_number);    
                    until (bd_decimal_number <= '11111111');

                    exibe_decimal(binary_to_decimal(bd_decimal_number));
                end;

                0: quit := true;
            end;
        until (quit = True);
    end;

    function decimal_to_binary(decimal_value: integer): TArrayShortInteger;
        Var
            i:integer;
        
        Begin
            //Para o for, estou fazendo a incrementação do começo para o fim.
            //assim, quando executar o writeLn, naõ precise correr ao contrário
            for i := Low(decimal_to_binary) to High(decimal_to_binary) do
            begin
                if (decimal_value mod 2) = 0 then //Verificando se o resto é 0
                    begin
                    decimal_to_binary[i] := 0;
                    end
                else //Caso não seja 0, será 1.
                    begin
                    decimal_to_binary[i] := 1;
                    end;
                
                //Fazendo a divisão real do valor
                decimal_value := decimal_value div 2;
            end;
        End;  

    function binary_to_decimal(bd_decimal_number:String): Real;
     Var
        aux: real;
        bd_binary_convertion: Integer;
        i: integer;

    Begin
        {Limpando a função antes de iniciar}
        binary_to_decimal := 0;

        for i := Low(bd_decimal_number) to High(bd_decimal_number) do
        begin
            if bd_decimal_number[i] = '1' then
            begin
                binary_to_decimal := binary_to_decimal + power(2, (Length(bd_decimal_number)-i));
            end;
        end;
    end;

    function somatoria_init(): TArrayShortInteger;
    Var
        primeiro_byte, segundo_byte: TString;
        round_primeiro_byte, round_segundo_byte, resultado_final: Integer;
        i: integer;
    
    begin
        {Coletando os dados }
        write('Primeiro Valor: ');
        readln(primeiro_byte);

        write('Segunda Valor: ');
        readln(segundo_byte);

        {Arredondamento dos Valores passados e convertidos}
        round_primeiro_byte := Round(binary_to_decimal(primeiro_byte));
        round_segundo_byte := Round(binary_to_decimal(segundo_byte));

        {Realizando a soma decimal dos valores}
        resultado_final :=  round_segundo_byte + round_primeiro_byte;

        {Convertendo a soma em binária}
        somatoria_init := decimal_to_binary(resultado_final);
        
        {Exibindo os resultados}
        write('Resultado: ');
        for i := High(somatoria_init) downto Low(somatoria_init) do
        begin
            write(somatoria_init[i]);
        end;
        readln();
    end;
end.
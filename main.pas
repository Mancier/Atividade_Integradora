Program main;

Uses 
    convertions, calc_logical, Crt, SysUtils;

Var
    choice: byte;
    quit: boolean;

BEGIN
    quit := false;

    repeat
        ClrScr; {Limpando a tela antes do bom e velho pascal compilar}
        writeln('Bem Vindo ao Nosso Projeto Integrador');
        writeln('Opcoes:');
        writeln('  1 => Conversao');
        writeln('  2 => Soma Binaria');
        writeln('  3 => Calculadora Logica');
        writeln('  4 => Sobre');
        writeln('  0 => Sair');        
        writeln();
        write('Escolha o que deseja fazer: ');
        readln(choice);

        Case choice Of 
            1: convertion_construct;
            2: somatoria_init;
            3: calc_logical_construct;
            4:begin
                ClrScr;
                writeln('Desenvolvido com muito amor e cafe por...');
                writeln('Ana Laura');
                writeln('Andre Toledo');
                writeln('Victor Augusto');
                writeln('==========================');
                writeln('Que a forca esteja com voce');
                readln;
            end;
            0: quit := true;
        End;
    until (quit = true);
END.